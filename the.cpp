///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file the.cpp
/// @version 2.0
///
/// Unit test for this lab
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06_04_2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <cassert>

#include "cat.hpp"
#include "list.hpp"

using namespace std;

int main(){
   cout << "Hello World" << endl;

   Node node;              //instantiate a node
   DoubleLinkedList list;  //instantiate a DoubleLinkedList
   
   cout << "Is it empty: " << boolalpha << list.empty() << endl;
   
   //test push_front
   list.push_front(new Node());
   cout << "Added node" << endl;
   cout << "   Is it empty: " << boolalpha << list.empty() << endl;
   list.push_front(new Node());
   cout << "Added another node" << endl;
   
   //test size
   cout << "   Size: " << list.size() << endl;
   
   //test pop_front
   list.pop_front();
   cout << "Removed node" << endl;
   cout << "   Size: " << list.size() << endl;
   
   //test get_first
   cout << "   First node: " << list.get_first() << endl;

   list.push_front(new Node());
   list.push_front(new Node());
   cout << "Added 2 nodes" << endl;
   cout << "   Size: " << list.size() << endl;  
   cout << "   Is it empty: " << boolalpha << list.empty() << endl;
   
   //test push_back
   list.push_back(new Node());
   cout << "Added node to back" << endl;
   cout << "   Size: " << list.size() << endl;
   cout << "   Is it empty: " << boolalpha << list.empty() << endl;
   
   //test pop_back
   list.pop_back();
   cout << "Removed node from back" << endl;
   cout << "   Size: " << list.size() << endl;

   //test get_last
   cout << "   Last node: " << list.get_last() << endl;
   
   return 0;

}
