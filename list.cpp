///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// 
/// 
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06_04_2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <cstdlib>
#include <cassert>

#include "list.hpp" 
using namespace std;

//return true if list is empty
const bool DoubleLinkedList::empty() const{ 
   if( head == nullptr && tail == nullptr )
      return true;
   else
      return false;  
}

//add newNode to front of list
void DoubleLinkedList::push_front(Node* newNode){
   if(newNode == nullptr)
      return;              //if newNode is nothing, then quietly return
   if(head != nullptr){    //if list isn't empty
      newNode->next = head;
      newNode->prev = nullptr;
      head->prev = newNode;
      head = newNode;   
   }else{                  //if list is empty
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   amntNodes++;
}

void DoubleLinkedList::push_back(Node* newNode){
   if(newNode == nullptr)
      return;              //if newNode is nothing, quietly return
   if(tail != nullptr){    //if list isn't empty
      newNode->next = nullptr;
      newNode->prev = tail;
      tail->next = newNode;
      tail = newNode;
   }else{
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   amntNodes++;
}

//remove node from the front, if list is empty return nullptr
Node* DoubleLinkedList::pop_front(){
   if(head == nullptr )
      return nullptr;      //if empty return nullptr
   if(head == tail){       //if list only has one
      Node* returns = head;
      head = nullptr;
      tail = nullptr;
      amntNodes--;
      return returns;
   }else{
      Node* returns = head;
      head = head->next;
      head->prev = nullptr;
      returns->next = nullptr;
      amntNodes--;
      return returns;
   }
}

Node* DoubleLinkedList::pop_back(){
   if(head == nullptr )
      return nullptr;      //if empty return nullptr
   if(head == tail){       //if list only has one
      Node* returns = tail;
      head = nullptr;
      tail = nullptr;
      amntNodes--;
      return returns;
   }else{
      Node* returns = tail;
      tail = tail->prev;
      tail->next = nullptr;
      returns->prev = nullptr;
      amntNodes--;
      return returns;
   }
}

//return first node from the list, don't make changes to the list
Node* DoubleLinkedList::get_first() const{
   return head;
}

Node* DoubleLinkedList::get_last() const{
   return tail;
}

//return node immediately following currentNode
Node* DoubleLinkedList::get_next(const Node* currentNode) const{
   if(head == nullptr)     //if list is empty
      return nullptr;      
   return currentNode->next;
}

Node* DoubleLinkedList::get_prev(const Node* currentNode) const{
   if(head == nullptr)     //if list is empty
      return nullptr;      
   return currentNode->prev;

}

//return number of nodes in list
//size in header file

void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode){
   if(currentNode == nullptr && head == nullptr){
      push_front(newNode);
      return;
   }
   
   if(currentNode != nullptr && head == nullptr){
      assert(false);       //can't have currentNode if list is empty
   }

   if(currentNode == nullptr && head != nullptr){
      assert(false);       //throw error, something's not right
   }
   
   //at this point, currentNode != null && head != null
   assert(currentNode != nullptr && head != nullptr);
   
   //make sure currentnode is in the list
   assert(isIn(currentNode));
   
   //make sure that newNode isn't in the list
   assert(!isIn(newNode));

   if(tail != currentNode){
      newNode->prev = currentNode;
      newNode->next = currentNode->next;
      currentNode->next->prev = newNode;
      currentNode->next = newNode;
   }else{
      push_back(newNode);
   }
   validate();
}

void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode){
   if(currentNode == nullptr && head == nullptr){
      push_front(newNode);
      return;
   }
   
   if(currentNode != nullptr && head == nullptr){
      assert(false);
   }

   if(currentNode == nullptr && head != nullptr){
      assert(false);
   }

   assert(currentNode != nullptr && head != nullptr);
   
   assert(isIn(currentNode));
   
   assert(!isIn(newNode));
   
   if(head != currentNode){
      newNode->prev = currentNode->prev;
      currentNode->prev = newNode;
      newNode->next = currentNode;
      newNode->prev->next = newNode;
   }else{
      push_front(newNode);
   }
   validate();
}


const bool DoubleLinkedList::isIn(Node* aNode) const{
   Node* currentNode = head;
   while(currentNode != nullptr){
      if(currentNode == aNode)
         return true;
      currentNode = currentNode->next;
   }
   return false;
}

void DoubleLinkedList::swap(Node* node1, Node* node2){
   assert(!empty());
   
   assert(node1 != nullptr);
   assert(node2 != nullptr);

   assert(isIn(node1));
   assert(isIn(node2));

   validate();

   if(node1 == node2){
      return;
   }

   Node* node1_left = node1->prev;
   Node* node1_right = node1->next;
   Node* node2_left = node2->prev;
   Node* node2_right = node2->next;

   bool isAdjoining = (node1_right == node2);
   
   //fixup node1_left
   if(!isAdjoining)              //case nodes not adjoining
      node1->prev = node2->prev;
   else
      node1->prev = node2;

   if(node1 != head){            //case node1 not first in the list
      node1_left->next = node2;
      node2->prev = node1_left;
   }else{                        //node1 first in the list
      head = node2;
      node2->prev = nullptr;
   }

   //fixup node2_right
   if(!isAdjoining)
      node2->next = node1_right; //case nodes not adjoining
   else
      node2->next = node1;

   if(node2 != tail){            //case node2 not last in list
      node2_right->prev = node1;
      node1->next = node2_right;
   }else{
      tail = node1;
      node1->next = nullptr;
   }

   //fixup inside connectors
   if(!isAdjoining){
      node1_right->prev = node2;
      node2_left->next = node1;
   }

   validate();   
}

const bool DoubleLinkedList::isSorted() const {
   if(amntNodes <= 1) //if list is empty or only has 1 item
      return true;

   for(Node* i = head; i->next != nullptr; i = i->next){
      if( *i > *i->next)
         return false;
   }
   return true;
}

void DoubleLinkedList::insertionSort(){       //this function depends on Node's < operator
   validate();

   if(amntNodes <= 1)         //if list is empty or has 1 item
      return;

   for(Node* i = head; i->next != nullptr; i = i->next){
      Node* minNode = i;
      for(Node* j = i->next; j != nullptr; j = j->next){
         if(*minNode > *j)
            minNode = j;
      }
      
      swap(i,minNode);
      i = minNode;            //fixes i so i->next will work
   }
   validate();
}

bool DoubleLinkedList::validate() const{
   if(head == nullptr){
      assert(tail == nullptr);
      assert(amntNodes == 0);
      assert(empty());
   }else{
      assert(tail != nullptr);
      assert(amntNodes != 0);
      assert(!empty());
   }

   if(tail == nullptr){
      assert(head == nullptr);
      assert(amntNodes == 0);
      assert(empty());
   }else{
      assert(head != nullptr);
      assert(amntNodes != 0);
      assert(!empty());
   }
   
   if(head != nullptr && tail == head){
      assert(amntNodes == 1);
   }

   unsigned int fCount = 0;
   Node* currentNode = head;
   //count forward through list
   while(currentNode != nullptr){
      fCount++;
      if(currentNode->next != nullptr){
         assert(currentNode->next->prev == currentNode);
      }
      currentNode = currentNode->next;
   }
   assert(amntNodes == fCount);

   //count backward through list
   unsigned int bCount = 0;
   currentNode = tail;
   while(currentNode != nullptr){
      bCount++;
      if(currentNode->prev != nullptr){
         assert(currentNode->prev->next == currentNode);
      }
      currentNode = currentNode->prev;
   }
   assert(amntNodes == bCount);
   
   return true;
}
