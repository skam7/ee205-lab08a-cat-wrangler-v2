///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06_04_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

using namespace std;

#include <string>
#include "node.hpp"

class DoubleLinkedList{
   protected:
      Node* head = nullptr;
      Node* tail = nullptr;
      unsigned int amntNodes = 0;

   public:
      const bool empty() const;
      void push_front(Node* newNode);
      void push_back(Node* newNode);
      
      Node* pop_front();
      Node* pop_back();
      Node* get_first() const;
      Node* get_last() const;
      
      Node* get_next(const Node* currentNode) const;
      Node* get_prev(const Node* currentNode) const;

      inline unsigned int size() const { return amntNodes; };    
      
      void insert_after(Node* currentNode, Node* newNode);
      void insert_before(Node* currentNode, Node* newNode);
      
      const bool isIn(Node* aNode) const; //true if aNode is in the list
      void swap(Node* node1, Node* node2);
      
      const bool isSorted() const;  //this function depends on Node's < operator
      void insertionSort();       //this function depends on Node's < operator
      
      //void dump() const;
      bool validate() const;
};
